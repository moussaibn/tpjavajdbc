package org.Moussa.Serie1.Exercice1.Utils;

import org.Moussa.Serie1.Exercice1.Model.Civility;
import org.Moussa.Serie1.Exercice1.Model.Maire;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnalyseFichierMaires {
    public static void main(String[] args) throws SQLException {
        Function<String, Maire> parseMaire = x -> {
            String[] split = x.split(";");
            String codeDept = split[0];
            String codeIns = split[2];
            String last_name = split[5];
            String first_name = split[6];
            Civility civility = Civility.of(split[7]);
            String date_of_birth = split[8];
            if (codeDept.length() == 1) {
                codeDept = "0" + codeDept;
            }
            if (codeIns.length() == 1) {
                codeIns = "00" + codeIns;
            } else if (codeIns.length() == 2) {
                codeIns = "0" + codeIns;
            }
            String codePostale = codeDept + codeIns;
            return new Maire(codePostale, codeIns, last_name, first_name, civility, date_of_birth);
        };

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "tp-jdbc-user", "user");
        String sqlMaire = "insert into Maire(first_name,last_Name,civility,date_of_birth,code_insee) values (?, ?,?, ?,?)";
        PreparedStatement preparedStatementMaire = connection.prepareStatement(sqlMaire);

        Path path = Path.of("data-tp/maires-25-04-2014.csv");
        try (BufferedReader br = Files.newBufferedReader(path)) {
            Map<String, Maire> maires = br.lines().filter(l -> !l.startsWith("Code"))
                    .map(parseMaire).
                            collect(Collectors.toMap(Maire::getCodePostale,
                                    Function.identity(),
                                    (maire1, maire2) -> {
                                        System.out.println("doublon = " + maire1.getFirst_name());
                                        return maire1;
                                    }
                                    )
                            );
            //Creation des requetes
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            for (Map.Entry<String, Maire> entry : maires.entrySet()) {
                preparedStatementMaire.setString(1, entry.getValue().getFirst_name());
                preparedStatementMaire.setString(2, entry.getValue().getLast_name());
                preparedStatementMaire.setString(3, entry.getValue().getCivility());
                preparedStatementMaire.setDate(4, new Date(format.parse(entry.getValue().getDate_of_birth()).getTime()));
                preparedStatementMaire.setString(5, entry.getValue().getCodeIns());
                preparedStatementMaire.addBatch();
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        int[] countM = preparedStatementMaire.executeBatch();

        int sumM = Arrays.stream(countM).sum();
        System.out.println("nombre de maires = " + sumM);
    }
}

