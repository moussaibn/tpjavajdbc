package org.Moussa.Serie1.Exercice1.Utils;

import org.Moussa.Serie1.Exercice1.Model.Commune;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnalyseFichierCommune {
    public static void main(String[] args) throws SQLException {
        Function<String, Commune> parseCommune = x -> {
            String[] split = x.split(";");
            String codeDept = split[0];
            String codeIns = split[2];
            String nom = split[3];
            String last_name = split[5];
            String first_name = split[6];
            int population = Integer.parseInt(split[4].trim());
            if (codeDept.length() == 1) {
                codeDept = "0" + codeDept;
            }
            if (codeIns.length() == 1) {
                codeIns = "00" + codeIns;
            } else if (codeIns.length() == 2) {
                codeIns = "0" + codeIns;
            }
            String codePostale = codeDept + codeIns;
            return new Commune(codePostale, nom, last_name, first_name,population);
        };

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "tp-jdbc-user", "user");
        String sqlCommune = "insert into commune(id,name) values (?, ?)";
        String sqlUpdateCommune = "Update commune inner join Maire on (Maire.First_name, Maire.last_name)=(?,?)  set  commune.id_maire = Maire.id  where commune.id = ? ";

        String sqlCommunePopulation = "update commune set population = ? where id = ?";

        PreparedStatement preparedStatementCommune = connection.prepareStatement(sqlCommune);

        Path path = Path.of("data-tp/maires-25-04-2014.csv");
        try (BufferedReader br = Files.newBufferedReader(path)) {
            Map<String, Commune> communes = br.lines().filter(l -> !l.startsWith("Code"))
                    .map(parseCommune).
                            collect(Collectors.toMap(Commune::getCodePostale,
                                    Function.identity(),
                                    (commune1, commune2) -> {
                                        System.out.println("doublon = " + commune1.getNom());
                                        return commune2;
                                    }
                                    )
                            );
            //Creation des requetes
            for (Map.Entry<String, Commune> entry : communes.entrySet()) {
                preparedStatementCommune.setString(1, entry.getValue().getCodePostale());
                preparedStatementCommune.setString(2, entry.getValue().getNom());
                preparedStatementCommune.addBatch();

            }
            //preparedStatementCommune.executeBatch();
            preparedStatementCommune = connection.prepareStatement(sqlUpdateCommune);
            for (Map.Entry<String, Commune> entry : communes.entrySet()) {
                preparedStatementCommune.setString(1, entry.getValue().getFirst_name());
                preparedStatementCommune.setString(2, entry.getValue().getLast_name());
                preparedStatementCommune.setString(3, entry.getValue().getCodePostale());
                preparedStatementCommune.addBatch();

            }
            //
            preparedStatementCommune = connection.prepareStatement(sqlCommunePopulation);
            for (Map.Entry<String, Commune> entry : communes.entrySet()) {
                preparedStatementCommune.setInt(1, entry.getValue().getPopulation());
                preparedStatementCommune.setString(2, entry.getValue().getCodePostale());

                preparedStatementCommune.addBatch();

            }

            int[] countC = preparedStatementCommune.executeBatch();
            int sumC = Arrays.stream(countC).sum();
            System.out.println("nombre de communes = " + sumC);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
