package org.Moussa.Serie1.Exercice1.Model;

public class Departement {
    private String codeDept ;
    private String nomDept;
    public Departement(String codeDept, String nomDept) {
        this.codeDept=codeDept;
        this.nomDept=nomDept;
    }

    public String getCodeDept() {
        return codeDept;
    }

    public void setCodeDept(String codeDept) {
        this.codeDept = codeDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
}
