package org.Moussa.Serie1.Exercice1.Model;

public class Commune {

    private String nom;
    private String first_name;
    private String last_name;
    private String codePostale;
    private int population;

    public Commune(String codePostale, String nom,String last_name,String first_name, int population) {
        this.codePostale = codePostale;
        this.nom = nom;
        this.first_name = first_name;
        this.last_name = last_name;
        this.population = population;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(String codePostale) {
        this.codePostale = codePostale;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "Commune[" +
                "nom='" + nom + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", codePostale='" + codePostale + '\'' +
                ", population=" + population +
                ']';
    }
}
