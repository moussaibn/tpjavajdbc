package org.Moussa.Serie1.Exercice1.Model;

public class Maire {
    private  String codePostale;
    private String codeIns;
    private String last_name;
    private String first_name;
    private Civility civility;
    private String date_of_birth;
    public Maire(){}
    public Maire(String codePostale, String codeIns, String last_name, String first_name, Civility civility, String date_of_birth) {
        this.codePostale=codePostale;
        this.codeIns = codeIns;
        this.last_name = last_name;
        this.first_name = first_name;
        this.civility = civility;
        this.date_of_birth = date_of_birth;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public String getCodeIns() {
        return codeIns;
    }

    public void setCodeIns(String codeIns) {
        this.codeIns = codeIns;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getCivility() {
        return civility.toString();
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    @Override
    public String toString() {
        return "Maire{" +
                "codePostale='" + codePostale + '\'' +
                ", codeIns='" + codeIns + '\'' +
                ", last_name='" + last_name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", civility='" + civility + '\'' +
                ", date_of_birth='" + date_of_birth + '\'' +
                '}';
    }
}
