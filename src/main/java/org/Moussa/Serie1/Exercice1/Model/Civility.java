package org.Moussa.Serie1.Exercice1.Model;

import java.util.Arrays;

public enum Civility {
    M("M"),
    MLLE("Mlle"),
    MME("Mme");
    private String label;
    private Civility(String label){
        this.label=label;
    }

    public static Civility of(String label){
        return Arrays.stream(values()).
                filter(value -> value.label.equals(label))
                .findFirst().orElseThrow();
    }

}
