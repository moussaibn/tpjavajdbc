package org.Moussa.Serie1.Exercice2;

import org.Moussa.Serie1.Exercice1.Model.Departement;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ImportDepartement {

    public static void main(String[] args) throws SQLException {
        Function<String, Departement> parseDepartement = x -> {
            String[] split = x.split(";");
            String codeDept = split[0];
            String nomDept = split[1];

            if (codeDept.length() == 1) {
                codeDept = "0" + codeDept;
            }

            return new Departement(codeDept, nomDept);
        };

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "tp-jdbc-user", "user");
        String sqlDepartement = "insert into Departement(id,name) values (?, ?)";
        String sqlAddDepartement = "Update commune set  commune.id_departement= ?  where commune.id like ?";

        PreparedStatement preparedStatementCommune = connection.prepareStatement(sqlDepartement);

        Path path = Path.of("data-tp/departement.csv");
        try (BufferedReader br = Files.newBufferedReader(path)) {
            Map<String, Departement> communes = br.lines().filter(l -> !l.startsWith("Code"))
                    .map(parseDepartement).
                            collect(Collectors.toMap(Departement::getCodeDept,
                                    Function.identity(),
                                    (dept1, dept2) -> {
                                        System.out.println("doublon = " + dept1.getNomDept());
                                        return dept1;
                                    }
                                    )
                            );
            //Creation des requetes

            for (Map.Entry<String, Departement> entry : communes.entrySet()) {
                preparedStatementCommune.setString(1, entry.getValue().getCodeDept() );
                preparedStatementCommune.setString(2, entry.getValue().getNomDept());
                preparedStatementCommune.addBatch();

            }

            int[] countC = preparedStatementCommune.executeBatch();
            int sumC = Arrays.stream(countC).sum();
            System.out.println("nombre de departement = " + sumC);
            preparedStatementCommune = connection.prepareStatement(sqlAddDepartement);

            for (Map.Entry<String, Departement> entry : communes.entrySet()) {
                preparedStatementCommune.setString(1, entry.getValue().getCodeDept());
                preparedStatementCommune.setString(2, entry.getValue().getCodeDept() + "%");

                preparedStatementCommune.addBatch();

            }
            int[] count = preparedStatementCommune.executeBatch();
            int sum = Arrays.stream(count).sum();
            System.out.println("nombre de communes modifié = " + sum);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
