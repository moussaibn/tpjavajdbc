package org.Moussa.Serie1.Exercice3;

import org.Moussa.Serie1.Exercice1.Model.Civility;
import org.Moussa.Serie1.Exercice1.Model.Departement;
import org.Moussa.Serie1.Exercice1.Model.Maire;

import java.sql.*;

public class MaireUtils {
    static Connection connection;

    public static long countMaire() throws SQLException {
        String sql1 = "select count(*) count from Maire";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql1);
        resultSet.next();
        return resultSet.getLong("count");
    }


    public static long countMaire(String civility) throws SQLException {
        String sql1 = "select count(*) count from Maire where civility =?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql1);
        preparedStatement.setString(1, civility);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getLong("count");
    }

    public static Maire getOldestMaire() throws SQLException {
        String sql = "select * from Maire Order by date_of_birth ASC limit 1";
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        resultSet.next();
        Maire maire = new Maire();
        maire.setFirst_name(resultSet.getString("first_name"));
        maire.setLast_name(resultSet.getString("last_name"));
        maire.setCivility(Civility.of(resultSet.getString("civility")));
        maire.setCodeIns(resultSet.getString("code_insee"));
        maire.setDate_of_birth(resultSet.getDate("date_of_birth").toString());
        return maire;
    }

    public static long countDepartementPopulation(Departement departement) throws SQLException {
        String sql = "select  sum(population) population FROM commune  where id like ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, departement.getCodeDept() + "%");
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getLong("population");

    }


    public static long countPopulation() throws SQLException {
        String sql = "select  sum(population) population FROM commune ";
        ResultSet resultSet = connection.createStatement().executeQuery(sql);
        resultSet.next();
        return resultSet.getLong("population");
    }

    public static long countMaire(int year) throws SQLException {
        String sql = "select count(*) nbMaire FROM Maire where year(date_of_birth) = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, "" + year);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getLong("nbMaire");
    }

    public static long countCivilityByAge(Civility civility, int age) throws SQLException {
        String sql = "select count(*) nbMaire FROM Maire where year(date_of_birth) = (2019 - ?) and civility =?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, "" + age);
        preparedStatement.setString(2, "" + civility.toString());
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return resultSet.getLong("nbMaire");
    }

}
