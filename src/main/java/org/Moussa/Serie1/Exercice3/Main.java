package org.Moussa.Serie1.Exercice3;

import org.Moussa.Serie1.Exercice1.Model.Civility;
import org.Moussa.Serie1.Exercice1.Model.Departement;
import org.Moussa.Serie1.Exercice1.Model.Maire;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        try {
            MaireUtils.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_jdbc", "tp-jdbc-user", "user");

            //Question 1:
            long nbMaires = MaireUtils.countMaire();
            System.out.println("R1- Le nombre de Maire dans la base est  " + nbMaires);

            //Question 2:
            nbMaires = MaireUtils.countMaire("Mme");
            System.out.println("R2- Le nombre de femmes Maire dans la base est  " + nbMaires);

            //Question 3:
            Maire maire = MaireUtils.getOldestMaire();
            System.out.println("R3- Le maire le plus vienx est " + maire.getCivility() + ". " + maire.getFirst_name() + " "
                    + maire.getLast_name() + " né(e) le " + maire.getDate_of_birth());

            //Question 4:
            Departement departement = new Departement("03", "ALLIER");
            long population = MaireUtils.countDepartementPopulation(departement);
            System.out.println("R4- La population du departement " + departement.getNomDept() + " est de " + population);

            //Question 5:
            long populationTotale = MaireUtils.countPopulation();
            System.out.println("R5- La population de toutes les communes est de  " + populationTotale);

            //Question 6:
            long nbMaire = MaireUtils.countMaire(1960);
            System.out.println("R6- Le nombre de maires nés en 1960 est " + nbMaire);

            //Question 7:
            Civility civility = Civility.MME;
            int age = 55;
            nbMaire = MaireUtils.countCivilityByAge(civility, age);
            System.out.println("R7- Le nombre de maires ayant la civilité " + civility + " et ayant " + age + " ans est de " + nbMaire);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
