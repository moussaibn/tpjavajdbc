# Serie 1

## Exercice 3:
> Question 1 :

La methode ``countMaire()`` qui retourne le nombre de maires en base.
```
R1- Le nombre de Maire dans la base est  35884
```
> Question 2 :

Une méthode ``countMaire(Civility)`` qui retourne le nombre de maires en fonction de la civilité passée en paramètre.
```
R2- Le nombre de femmes Maire dans la base est  5670
```
> Question 3 :

Une méthode ``getOldestMaire()`` qui retourne le maire le plus vieux de la base.
```
R3- Le maire le plus vienx est M. Marcel BERTHOME né(e) le 1922-04-04
```
> Question 4 :

Une méthode ``countDepartementPopulation(Departement)`` qui retourne la population du département passé en paramètre.
```
R4- La popullation du departement ALLIER est de 342729
```
> Question 5 :

Une méthode ``countPopulation()`` qui retourne la population totale de toutes les communes.
```
R5- La population de toutes les communes est 64347610
```
> Question 6 :

Une méthode ``countMaire(int year)`` qui retourne le nombre de maires nés l’année passée en paramètre.
```
R6- Le nombre de maires nés en 1960 est 1106
```
> Question 7 :

Une méhode ``countCivilityByAge(Civility civility, int age)`` qui retourne le nombre de maires ayant la civilité et l’âge passés en paramètre.
```
R7- Le nombre de maires ayant la civilité MME et ayant 55 ans est de 144
```